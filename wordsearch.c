/*
Michael Ashmead, Alex Owen, 600.120, 2/27/15, Homework 3, 484-682-9355, mashmea1, ashmeadmichael@gmail.com
*/

#define _GNU_SOURCE

#include <stdbool.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "wordsearch.h"

//searches through wordlist looking for generated word from grid
bool binarySearch(int numOfWords, char word[], char *wordlist[]) {
   int first = 0;
   int last = numOfWords - 1;
   int middle = (first + last) / 2;

   while (first <= last) {
      if (strcmp(wordlist[middle], word) < 0)
         first = middle + 1;
      else if (strcmp(wordlist[middle], word) == 0)
         return true;
      else
         last = middle - 1;
      middle = (first + last) / 2;
   }
   return false;
}

//reverses the word
void reverse(char (*word)[]) {
   char c;
   int len = strlen(*word);

   for (int x = 0; x < len/2; x++) {
      c = (*word)[x];
      (*word)[x] = (*word)[len - x - 1];
      (*word)[len - x - 1] = c;
   }
}

//generate all possible words going up
void generateUpWords(int rows, int cols, char grid[rows][cols], int numOfWords, char* wordlist[]) {
   char word[rows+1];
   for (int w = 0; w < cols; w++) {
      for (int x = 0; x < rows; x++) { //start row
         for (int y = 1; y <= rows; y++) { //wordlength
            for (int z = x; z < y; z++) {
               word[z-x] = grid[z][w];
            }
            word[y] = '\0';
            if (x < y) {
               reverse(&word);
               if (binarySearch(numOfWords, word, wordlist))
                  printf("%s %i %i %c\n", word, x, w, 'U');
            }
         }
      }
   }
}

//generate all possible words going right and check if they are in the wordlist
void generateRightWords(int rows, int cols, char grid[rows][cols], int numOfWords, char *wordlist[]) {
   char word[cols+1];
   for (int w = 0; w < rows; w++) {
      for (int x = 0; x < cols; x++) { //start col
         for (int y = 1; y <= cols; y++) { //wordlength
            for (int z = x; z < y; z++) {
               word[z-x] = grid[w][z];
            }
            word[y] = '\0';
            if ((x < y) && binarySearch(numOfWords, word, wordlist))
               printf("%s %i %i %c\n", word, w, x, 'R');
         }
      }
   }
}

//generate all possible words going down
void generateDownWords(int rows, int cols, char grid[rows][cols], int numOfWords, char *wordlist[]) {
   char word[rows+1];
   for (int w = 0; w < cols; w++) {
      for (int x = 0; x < rows; x++) { //start row
         for (int y = 1; y <= rows; y++) { //wordlength
            for (int z = x; z < y; z++) {
               word[z-x] = grid[z][w];
            }
            word[y] = '\0';
            if ((x < y) && binarySearch(numOfWords, word, wordlist))
               printf("%s %i %i %c\n", word, x, w, 'D');
         }
      }
   }
}

//generate all possible words going left
void generateLeftWords(int rows, int cols, char grid[rows][cols], int numOfWords, char* wordlist[]) {
   char word[cols+1];
   for (int w = 0; w < rows; w++) {
      for (int x = 0; x < cols; x++) { //start col
         for (int y = 1; y <= cols; y++) { //wordlength
            for (int z = x; z < y; z++) {
               word[z-x] = grid[w][z];
            }
            word[y] = '\0';
            if (x < y) {
               reverse(&word);
               if (binarySearch(numOfWords, word, wordlist))
                  printf("%s %i %i %c\n", word, w, x, 'L');
            }
         }
      }
   }
}

void setGrid(int rows, int cols, char grid[rows][cols], FILE *f) {
   int colcount = 0;
   int rowcount = 0;
   char ch;
   while((ch=fgetc(f))!= EOF) {
      if (ch == '\n') {
         rowcount++;
         colcount=0;
      }
      else {
         grid[rowcount][colcount] = ch;
         colcount++;
      }
   }
}

void expandWordArray(char **pra, int *psize) {
   (*psize) *= 2;
   *pra = realloc(*pra, (*psize) * sizeof(char));
}

void expandWordlistArray(char ***pra, int *psize) {
   (*psize) *= 2;
   *pra = realloc(*pra, (*psize) * sizeof(char *));
}

bool checkValid(int *rows, int *cols, FILE *f) {
   bool startofline = false;
   char tempChar = NULL;
   while ((tempChar = fgetc(f)) != EOF) {
      startofline = false;
      if (tempChar != '\n' && (*rows) == 0) {
         (*cols)++;
      }
      else if (tempChar == '\n') {
         (*rows)++; 
         startofline = true;
      }
   }
   if (!startofline)
      (*rows)++;

   fseek(f, 0, SEEK_SET);
   int coltemp = 0;
   while ((tempChar = fgetc(f)) != EOF) {
      if (tempChar != '\n') {
         coltemp++;
      }
      else {
         if (coltemp != (*cols))
            return false;
         coltemp=0;
      }
   }
   if ((!startofline) && (coltemp != (*cols)))
      return false;
		

   if ((*rows) == 0 || (*cols) == 0) {
      return false;
   }
   return true;
}
