/*
Michael Ashmead, Alex Owen, 600.120, 2/27/15, Homework 3, 484-682-9355, mashmea1, ashmeadmichael@gmail.com
*/

#define _GNU_SOURCE

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include "wordsearch.h"

int main(int argc, char* argv[]) {
   if (argc < 3) {
      printf("Please enter two file names: Ex: grid.txt wordlist.txt\n");
      return 1;
   }

   FILE *f = fopen(argv[2], "r");
 
   int wordlistSize = DEFAULT_SIZE;
   int wordlistIndex = 0;
   char **wordlist = malloc(wordlistSize * sizeof(char *));

   int wordSize = DEFAULT_SIZE;
   int wordIndex = 0;
   int numOfWords = 0;
   char tempChar;
   char previousChar = NULL; //needed to fix bug for no '\n' at end of file
   char *tempWord = malloc(wordSize * sizeof(char));
   while((tempChar = fgetc(f)) != EOF) {
      if (wordlistIndex >= wordlistSize) //expand wordlist array if it won't be able to fit the next word
         expandWordlistArray(&wordlist, &wordlistSize);
      if (wordIndex >= (wordSize - 2)) //expand word array if it won't be able to fit the next character
         expandWordArray(&tempWord, &wordSize);

      if (tempChar != '\n') {
         tempWord[wordIndex++] = tempChar;
      }
      else {
         tempWord[wordIndex] = '\0';
         wordlist[wordlistIndex++] = strcpy(malloc(strlen(tempWord)+1), tempWord);
         wordIndex = 0;
         numOfWords++;
      }
      previousChar = tempChar;
   }
   if (tempChar != '\n') { //needed to fix bug for no '\n' at end of file
      tempWord[wordIndex-1] = previousChar;
      tempWord[wordIndex++] = '\0';
      wordlist[wordlistIndex++] = strcpy(malloc(strlen(tempWord)+1), tempWord);
      numOfWords++;
   }
   fclose(f);
   
   //sort wordlist alphabetically using bubble sort
   bool swapped = true;
   int j = 0;
   char *tmp;
   while (swapped) {
      swapped = false;
      j++;
      for (int i = 0; i < numOfWords - j; i++) {
         //first word comes after second word
         if (strcmp(wordlist[i], wordlist[i+1]) > 0) {
            tmp = wordlist[i];
            wordlist[i] = wordlist[i+1];
            wordlist[i+1] = tmp;
            swapped = true;
         }
      }
   }

   //check that grid is valid and get number of rows and cols
   f = fopen(argv[1], "r");
   int rows = 0;
   int cols = 0;
   bool valid = checkValid(&rows, &cols, f);
   if (!valid) {
      printf("%s\n", "Grid is not valid!");
      return 1;
   }
   fclose(f);   

   //initialize an array of chars to store grid.txt
   char grid[rows][cols];

   f = fopen("grid.txt", "r");
   setGrid(rows, cols, grid, f);
   fclose(f);

   //generate all possible words going right and check if they are in the wordlist
   generateRightWords(rows, cols, grid, numOfWords, wordlist);

   //generate all possible words going down
   generateDownWords(rows, cols, grid, numOfWords, wordlist);

   //generate all possible words going left
   generateLeftWords(rows, cols, grid, numOfWords, wordlist);

   //generate all possible words going up
   generateUpWords(rows, cols, grid, numOfWords, wordlist);

   //free memory
   for (int x = 0; x < numOfWords; x++) {
      free(wordlist[x]);
   }
   if (wordlist)
      free(wordlist);

   return 0;
}
