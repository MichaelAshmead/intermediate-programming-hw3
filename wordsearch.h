//Michael Ashmead, Alex Owen, 600.120, 2/27/15, Homework 3, 484-682-9355, mashmea1, ashmeadmichael@gmail.com

#define DEFAULT_SIZE 5

//searches through wordlist looking for generated word from grid
bool binarySearch(int numOfWords, char word[], char *wordlist[]) ;

//reverses the word
void reverse(char (*word)[]) ;

//generate all possible words going up
void generateUpWords(int rows, int cols, char grid[rows][cols], int numOfWords, char* wordlist[]);

//generate all possible words going right and check if they are in the wordlist
void generateRightWords(int rows, int cols, char grid[rows][cols], int numOfWords, char *wordlist[]);

//generate all possible words going down
void generateDownWords(int rows, int cols, char grid[rows][cols], int numOfWords, char *wordlist[]);

//generate all possible words going left
void generateLeftWords(int rows, int cols, char grid[rows][cols], int numOfWords, char* wordlist[]);

//sets grid
void setGrid(int rows, int cols, char grid[rows][cols], FILE *f);

//expands word
void expandWordArray(char **pra, int *psize);

//expands wordlist
void expandWordlistArray(char ***pra, int *psize);

//check if grid is valid
bool checkValid(int *rows, int *cols, FILE *f);