Alex Owen JHED: aowen10
Michael Ashmeade: mashmea1

How the program works:

1) Take in words from wordlist.txt and store in array
2) Write a function that sorts the search words
3) Create another file that has sorted words
4) Write a function that reads in the grid that is given and stores the grid using dynamically allocated memory
5) Function that generates all possible "words" from the grid
6) Function that searches through wordlist.txt using "words" and prints matches


- Our test_wordsearch.c tests 3 functions: binarySearch, reverse, and expandWordArray
- Note: grid.txt and wordlist.txt should not have an empty line at the end
- Our program outputs repeats of words, however, in the implementation details it is said to be okay
-Use grid.txt and wordlist.txt as both the test files and normal input files
- Input: “./hw3 grid.txt wordlist.txt”

-The hardest part of the program was the dynamic memory allocation with our word array for the dictionary input.  We had trouble with expanding both a char* and our char** (the list of strings)
- It would have been a bigger help in terms of time spent on the program to have gone over valgrind later in the week.  Probably for most groups, valgrind was the last step.  Personally, I forgot most/ all of the function/workings of valgrind by the time I needed to use it.
- Could we go over class how to free all of the memory of dynarray.c? We ran it on the ugrad server and we still for 10 bytes (1 block) that needed to be freed, an error that came up on our program as well.

