/*
Michael Ashmead, Alex Owen, 600.120, 2/27/15, Homework 3, 484-682-9355, mashmea1, ashmeadmichael@gmail.com
*/

#define _GNU_SOURCE

#include <string.h>
#include <assert.h>
#include <stdio.h>
#include <stdbool.h>
#include "wordsearch.h"

int main(void) {
   printf("Testing Methods...\n");

   printf("Testing 'reverse'...\n");
   char word1[]= "forwards";
   char word2[]= "a";
   char word3[]= "qwertyuioplkjhgfdsazxcvbnm";
   reverse(&word1);
   reverse(&word2);
   reverse(&word3);
   assert(strcmp(word1, "sdrawrof") == 0);
   assert(strcmp(word2, "a") == 0);
   assert(strcmp(word3, "mnbvcxzasdfghjklpoiuytrewq") == 0);
   printf("'reverse' passed all tests.\n");

   printf("Testing 'expandWordArray'...\n");
   int wordSize = DEFAULT_SIZE;
   char *tempWord = malloc(wordSize * sizeof(char));
   char letters[] = {'a','b','c','d','e','f'};
   for (int x = 0; x < 6; x++) {
      if (x >= (wordSize - 2)) //expand word array if it won't be able to fit the next character
         expandWordArray(&tempWord, &wordSize);
      tempWord[x] = letters[x];
   }
   tempWord[6] = '\0';
   assert(strcmp(tempWord, "abcdef") == 0);
   wordSize = DEFAULT_SIZE;
   tempWord = malloc(wordSize * sizeof(char));
   char letters2[] = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p'};
   for (int x = 0; x < 16; x++) {
      if (x >= (wordSize - 2)) //expand word array if it won't be able to fit the next character
         expandWordArray(&tempWord, &wordSize);
      tempWord[x] = letters2[x];
   }
   tempWord[16] = '\0';
   assert(strcmp(tempWord, "abcdefghijklmnop") == 0);
   printf("'expandWordArray' passed all tests.\n");

   printf("Testing 'binarySearch'...\n");
   char *words[] = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p"};
   bool result1 = binarySearch(16, "h", words);
   assert(result1 == true);
   char *words2[] = {"a", "b", "c", "d", "e", "f"};
   bool result2 = binarySearch(6, "h", words2);
   assert(result2 == false);
   printf("'binarySearch' passed all tests.\n");
   
   printf("'hw3' passed all tests.\n");
}
